/******************************************************************************
 * Copyright (C) : 
 * 
 * File name      : wifi_parser.h
 * Description   : 
 * Author           : Deng Renjie [1951]
 * Date              : 2016/11/28 12:00
 * Modification : 
 ******************************************************************************/
#ifndef __WIFI_PARSER_H__
#define __WIFI_PARSER_H__
#include <iostream>
#include <string>
#include "../../util/util.h"

class CWifiParser
{
public:
    static void Parse(const struct pcap_pkthdr *header, const u_char *pkt_data);
};

#endif  /* __WIFI_PARSER_H__ */
