#include <stdio.h>
#include <iostream>
#include <pcap.h>
//#include <direct.h>  
#include <unistd.h>
#include <string.h>     //memset strncpy
#include <getopt.h>     //getopt_long
#include "util/util.h"
#include "util/my_log.h"
#include "PacketParser/Wifi/wifi_parser.h"

#define LINE_LEN 16
#define PCAP_ERRBUF_SIZE 256

//使用说明
char usage[] =

"\n"
"  %s - (C) 2015-2017 1951\n"
"  http://www.a-dd.cn\n"
"\n"
"  usage: %s [options] \n"
"\n"
"  options:\n"
"\n"
"      -i interface   : interface name \n"
"      -f file        : pcap file \n"

"\n"
"      --help             : Displays this usage screen\n"
"\n";


struct options
{
    int mode;                   /*  mode            */
    char *name;                 /*  interface name or pcap filename  */
}opt;

/* packet handler 函数原型 */
void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data);

//初始化
MY_RESULT Init()
{
    return MY_SUCCESS;
}

int  GetPacketFormDev()
{
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int i=0;
    pcap_t *adhandle;
    char errbuf[PCAP_ERRBUF_SIZE];/* 获取本机设备列表 */
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        return -1;
    }
    /* 打印列表 */
    for(d=alldevs; d; d=d->next)
    {
        ++i;
        //printf("%d. %s", ++i, d->name);
        if(0 == strcmp(opt.name,d->name))
        {
            //找到网卡设备，跳出循环
            printf("Found the interface.num[%d]name[%s]\n", i, d->name);
            break;
        }
    }

    if(i==0)
    {
        printf("\nNo interfaces found! Make sure WinPcap is installed.\n");
        return -1;
    }
   
     /* 打开设备 */

    /* open a device, wait until a packet arrives */   
    if ( (adhandle = pcap_open_live(d->name, 65535, 1, 0, errbuf)) == NULL)
    {
        fprintf(stderr,"\nUnable to open the adapter. %s is not supported by libpcap\n", d->name);
        pcap_freealldevs(alldevs);
        return -1;  
    }
    printf("\nlistening on %s(%s)...\n", d->name, d->description);
    /* 释放设备列表 */
    pcap_freealldevs(alldevs);

    /* wait loop forever */  
    int id = 0;  
    pcap_loop(adhandle, -1, packet_handler, (u_char*)&id);  
    pcap_close(adhandle); 

    return 0;
}/* 每次捕获到数据包时，libpcap都会自动调用这个回调函数 */

void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data)
{
//    //报文打印
//    printf("%d\n", header->caplen);  
//    for (bpf_u_int32 i = 0; i < header->caplen; ++i) 
//    {  
//      if (0 < i && 0 == i % 16) 
//          printf("\n");  
//      printf("%02x ", pkt_data[i]);  
//    }
    
    if (header->caplen < ETHER_HEAD_LEN)
    {
        return;  
    }

    CWifiParser::Parse(header,pkt_data);
}



int GetPacketFormFile(const char* file) 
{  
    if (NULL == file)
    {
        std::cout<<"The file point is NULL."<<std::endl;
        return -1;
    }

    char errbuf[100];      
    pcap_t *pfile = pcap_open_offline(file, errbuf);  
    if (NULL == pfile) {  
        printf("%s\n", errbuf);  
        return -1;  
    }   
    pcap_pkthdr *pkthdr = 0;  
    const u_char *pktdata = 0;  
    while(pcap_next_ex(pfile, &pkthdr, &pktdata) == 1)
    {
        CWifiParser::Parse(pkthdr,pktdata);
    }
    
    pcap_close(pfile);  
    return 0;  
}  

int main(int argc, char **argv)
{
    int i_opt;   
    int option_index = 0;  
    //init
    memset( &opt, 0, sizeof( opt ) );
    opt.mode = -1;

    static struct option long_options[] = {  
        {"interface",  required_argument, 0, 'i'},                  // 1 is required_argument 0 is NULL
        {"file",   required_argument, 0, 'f'}, 
        {"help",    0, 0, 'H'},
        {0, 0, 0, 0}  
    };  

    const char *optstring = "i:f:H"; 
    while ( (i_opt = getopt_long(argc, argv, optstring, long_options, &option_index)) != -1)  
    { 
        switch( i_opt )
        {
            case 0 :

                break;

            case ':' :

                printf("\"%s --help:\" for help.\n", argv[0]);
                return( 1 );

            case '?' :

                printf("\"%s --help?\" for help.\n", argv[0]);
                return( 1 );
            
            //模式设置 网卡模式
            case 'i' :
            
                if( opt.mode != -1 || opt.name != NULL )
                {
                    printf( "mode already specified.\n" );
                    printf("\"%s --help\" for help.\n", argv[0]);
                    return( 1 );
                }

                opt.name = optarg;
                opt.mode = 0;
                break;

            //模式设置 文件模式
            case 'f' :
            
                if( opt.mode != -1 || opt.name != NULL)
                {
                    printf( "mode already specified.\n" );
                    printf("\"%s --help\" for help.\n", argv[0]);
                    return( 1 );
                }

                opt.name = optarg;
                opt.mode = 1;
                break;
                
            case 'H' :
usage:
                printf( usage,argv[0], argv[0] );
                return( 1 );
                
            default : goto usage;
        }
    }  

    if( opt.mode == -1 )
    {
        printf( "Please specify a mode.\n" );
        printf("\"%s --help\" for help.\n", argv[0]);
        return( 1 );
    }

    //printf("\nmode[%d]name[%s]\n",opt.mode,opt.name);

    switch( opt.mode )
    {
        case 0 : return( GetPacketFormDev()     );
        case 1 : return( GetPacketFormFile(opt.name)   );
        default: 
            printf( "Can not recognize this mode.\n" );
            break;
    }
   
    return 0;
}