#include "my_log.h"

#define MAX_LOG_SIZE 5120 //日志缓存最大长度

using namespace std;

SINGLETON_IMPLEMENT(CMyLog);
void CMyLog::LogRecord(string strFunc,
    unsigned int uiLine,
    string strMod,
    int iErrCode,
    char* cFormat,
    ...)
{
    string strLog;
    strLog.clear();
    char acBuffer1[MAX_LOG_SIZE] = "\0", acBuffer2[MAX_LOG_SIZE] = "\0";
    //sprintf_s(acBuffer1, MAX_LOG_SIZE - 1, "[%s] [%d] [%s] [%d] ", strFunc.data(), uiLine, strMod.data(), iErrCode);
    int ret = snprintf(acBuffer1, MAX_LOG_SIZE - 1, "[%s] [%d]", strFunc.data(), uiLine);
    if(-1 == ret)
    {
        printf("sprintf_s is error.");
        return;
    }

    va_list args;
    va_start (args, cFormat);
    vsprintf(acBuffer2,cFormat, args);

    ofstream ofLogFile;
    ofLogFile.open(logFilePath, ios::out | ios::app);
    if (!ofLogFile)
    {
        return;
    }

    string strBuffer1, strBuffer2;
    strBuffer1 = acBuffer1;
    strBuffer2 = acBuffer2;

    ofLogFile.write(strBuffer1.data(), strBuffer1.size());
    ofLogFile.write(strBuffer2.data(), strBuffer2.size());
    ofLogFile.put('\n');
    ofLogFile.close();

    va_end(args);
}

void CMyLog::LogDebug(string strFunc,
    unsigned int uiLine,
    string strMod,
    int iErrCode,
    char* cFormat,
    ...)
{
    string strLog;
    strLog.clear();
    char acBuffer1[MAX_LOG_SIZE] = "\0", acBuffer2[MAX_LOG_SIZE] = "\0";

    //sprintf_s(acBuffer1, MAX_LOG_SIZE - 1, "[%s] [%d] [%s] [%d] ", strFunc.data(), uiLine, strMod.data(), iErrCode);
    int ret = snprintf(acBuffer1, MAX_LOG_SIZE - 1, "[%s][%d]:", strFunc.data(), uiLine);
    if(-1 == ret)
    {
        printf("sprintf_s is error.");
        return;
    }
    acBuffer1[MAX_LOG_SIZE - 1] = '\0'; 
    std::cout<<acBuffer1;
    va_list args;
    va_start (args, cFormat);
    vsprintf(acBuffer2,cFormat, args);

    acBuffer2[MAX_LOG_SIZE - 1] = '\0'; 
    std::cout<<acBuffer2<<std::endl;
    va_end(args);
}


void CMyLog::WriteData(
    char* cFormat,
    ...)
{
    char acBuffer2[MAX_LOG_SIZE] = "\0";


    va_list args;
    va_start (args, cFormat);
    vsprintf(acBuffer2,cFormat, args);

    ofstream ofLogFile;
    ofLogFile.open(DataFilePath, ios::out | ios::app);
    if (!ofLogFile)
    {
        return;
    }

    string  strBuffer2;
    strBuffer2 = acBuffer2;

    ofLogFile.write(strBuffer2.data(), strBuffer2.size());
    ofLogFile.put('\n');
    ofLogFile.close();

    va_end(args);
}