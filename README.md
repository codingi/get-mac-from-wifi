The get-mac-from-wifi project

    The get-mac-from-wifi project is based on the libpcap written by  Luis MartinGarcia for the tcpdump project.
    The aim is to provide set of tools for get mac address information transmitted by wifi equipment/devices.
    
Installation and usage

    Please see project's wiki https://gitlab.com/codingi/get-mac-from-wifi/wikis/home for information on installation and usage of get-mac-from-wifi.

Mailing list

    Current gr-gsm project's mailing list address is following:
        1310900046@qq.com

    Mailing list is a place for general discussions, questions about the usage and installation. In case of problem with installation please try to provide full information that will help reproducing it. Minimum information should contain:
        operating system with version,
        kind of installation ,
        version of libpcap ,
        error messages.

    To join the group with any e-mail addres use this address:
        1310900046@qq.com

Development

    New features are accepted through gitlab's pull requests. When creating pull request try to make it adress one topic (addition of a feature x, correction of bug y).
    If you wish to develop something for get-mac-from-wifi but don't know exactly what, then look for issues with label "Enhancement". Select one that you feel you are able to complete. After that claim it by commenting in the comment section of the issue. If there is any additional information about gr-gsm needed by you to make completing the task easier - just ask.

Videos
    
    no

Credits

    Peter Deng <1310900046@qq.com> - main author and project maintainer


Thanks

    This work is built upon the efforts made by many people to gather knowledge of wifi product.
